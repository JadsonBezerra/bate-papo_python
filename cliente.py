from socket import *
import threading


class minhaThread(threading.Thread):
    def __init__(self, funcao, args=None):
        threading.Thread.__init__(self)
        self.funcao = funcao
        self.args = args

    def run(self):
        if self.args is not None:
            self.funcao(self.args)
        else:
            self.funcao()


class tcpClient:
    def __init__(self, serverName='localhost', port=65000):
        self.serverName = serverName
        self.serverPort = port
        self.clientSocket = socket(AF_INET, SOCK_STREAM)
        self.clientSocket.connect((self.serverName, self.serverPort))
        self.remetente = "TODOS"

    def send(self):
        while True:
            msg = input()
            if msg == 'sair()':
                msgSend = self.codificar(self.remetente, 'SAIR', '')
                self.clientSocket.send(msgSend)
                break
            elif msg[0:5] == "nome(" and msg[-1] == ")":
                msgSend = self.codificar('SERVIDOR', 'NOME', msg[5:-1])
            elif msg[0:8] == "privado(" and msg[-1] == ")":
                msgSend = self.codificar('SERVIDOR', 'PRIVADO', msg[8:-1])
            elif msg == "lista()":
                msgSend = self.codificar('SERVIDOR', 'LISTA', '')
            elif msg == "sairPrivado()":
                msgSend = self.codificar(self.remetente, 'SAIRPRIV', '')
                self.remetente = 'TODOS'
            else:
                msgSend = self.codificar(self.remetente, '', msg)

            self.clientSocket.send(msgSend)

    def receive(self):
        while True:
            msgRec = self.clientSocket.recv(1024)
            tam, remetente, comando, dados = self.decodificar(msgRec)
            if comando == 'SAIR':
                msgSend = self.codificar('SERVIDOR', 'SAIR', '')
                self.clientSocket.send(msgSend)
                self.clientSocket.close()
                break
            elif comando == 'PRIVADO':
                self.remetente == 'SERVIDOR'
            elif dados[0:30] == "Conversa privada iniciada com ":
                self.remetente = dados[30:]
            elif dados == "Oferta de privado recusada!":
                self.remetente == 'TODOS'
            elif comando == 'SAIRPRIV':
                self.clientSocket.send(
                    self.codificar(self.remetente, 'SAIRPRIV', ''))
                self.remetente = 'TODOS'
            elif comando == 'MUDNPRIV':
                print(self.remetente+' mudou de nome para '+dados)
                self.remetente = dados
                continue
            print(dados)

    def run(self):

        threadSend = minhaThread(self.send)
        threadSend.daemon = True
        threadSend.start()
        threadRec = minhaThread(self.receive)
        threadRec.start()

    def codificar(self, remetente, comando, dados):
        tamanho = 2+8+8+len(dados)
        tamanhoCodificado = bytes(
            [(tamanho - (tamanho % 256))])+bytes([(tamanho % 256)])

        def transforma8bytes(x): return ('\x00'*(8-len(x))+x).encode('utf-8')

        remetenteCodificado = transforma8bytes(remetente)
        comandoCodificado = transforma8bytes(comando)

        mensagemCodificada = tamanhoCodificado + remetenteCodificado + \
            comandoCodificado + dados[0:80].encode('utf-8')
        return mensagemCodificada

    def decodificar(self, mensagem):
        tamanho = mensagem[0:2]
        mensagemDeco = mensagem.decode('utf-8')
        remetente = mensagemDeco[2:10]
        comando = mensagemDeco[10:18]
        dados = mensagemDeco[18:]

        def decode8bytes(x):
            for i in range(len(x)):
                if x[i] != '\x00':
                    return x[i:]
            return ''

        tamanhoDeco = sum(tamanho)
        remetenteDeco = decode8bytes(remetente)
        comandoDeco = decode8bytes(comando)
        dadosDeco = dados

        return tamanhoDeco, remetenteDeco, comandoDeco, dadosDeco


cliente = tcpClient()

cliente.run()
