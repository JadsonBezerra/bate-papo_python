from socket import *
import threading
import sys


class minhaThread(threading.Thread):
    def __init__(self, funcao, args=None):
        threading.Thread.__init__(self)
        self.funcao = funcao
        self.args = args

    def run(self):
        if self.args is None:
            self.funcao()
        else:
            self.funcao(self.args)


class tcpServer:
    def __init__(self, serverName='', port=65000):
        self.enderecos = {}
        self.clientes = {}
        self.serverName = serverName
        self.serverPort = port
        self.serverSocket = socket(AF_INET, SOCK_STREAM)
        self.serverSocket.bind((self.serverName, self.serverPort))
        self.serverSocket.listen(1)
        self.listaPrivados = []
        self.pararExecucao = False
        print("Sever disponivel")

    def run(self):
        threadClientes = minhaThread(self.listenClientes)
        threadClientes.daemon = True
        threadClientes.start()
        while True:
            self.listenServer()
            if(self.pararExecucao):
                self.serverSocket.close()
                break

    def listenServer(self):
        comandoServer = input()
        if comandoServer == "sair()":
            for sock in self.clientes.keys():
                sock.send(self.codificar('', 'SAIR', 'Servidor encerrando...'))
            self.pararExecucao = True
            exit()

    def listenClientes(self):
        while True:
            cliente, enderecoCliente = self.serverSocket.accept()
            mensagem = "Ola, diz teu nome ai"
            cliente.send(self.codificar('', '', mensagem))

            self.enderecos[cliente] = enderecoCliente

            def handleClient(cliente):
                nome = cliente.recv(1024)
                tamanho, remetente, comando, nome = self.decodificar(nome)

                nome = nome[0:8]

                while(nome in self.clientes.values()):
                    cliente.send(self.codificar(
                        '', '', ("Este nome ja esta em usao, tente novamente")))
                    nome = cliente.recv(1024)
                    tamanho, remetente, comando, nome = self.decodificar(nome)

                cliente.send(self.codificar(nome, '', ("Bem vindo, "+nome)))
                mensagemEntrada = "%s entrou na conversa!" % nome
                print(mensagemEntrada)
                self.clientes[cliente] = nome
                threadBroadcast = minhaThread(
                    self.broadcast, {'clienteBase': cliente, 'mensagem': mensagemEntrada})
                threadBroadcast.start()

                def lista():
                    def enviarLista():
                        listaTemp = ['<'+nomeCliente+('(Privado)' if self.estaEmPrivado(nomeCliente) else '')+', '+endereco[0]+', ' + str(endereco[1]) +
                                     '>' for nomeCliente, endereco in zip(self.clientes.values(), self.enderecos.values())]
                        print(listaTemp)
                        for item in listaTemp:
                            cliente.send(
                                self.codificar(nome, 'LISTA', item))

                    enviarListaThread = minhaThread(enviarLista)
                    enviarListaThread.start()

                def mudarnome():
                    novoNome = dados[0:8]
                    if(novoNome not in self.clientes.values()):
                        self.broadcast(
                            {'clienteBase': cliente, 'mensagem': nome+" agora é "+novoNome})
                        print(nome+" agora é "+novoNome)
                        if self.estaEmPrivado(nome):
                            for i in range(len(self.listaPrivados)):
                                for j in range(len(self.listaPrivados[i])):
                                    if self.listaPrivados[i][j] == self.clientes[cliente]:
                                        self.listaPrivados[i][j] = novoNome
                                        linhaPrivado = self.listaPrivados[i]
                                        ehBreak = True
                                        break

                                if ehBreak:
                                    break

                            nomeDestino = linhaPrivado[0] if linhaPrivado[0] != novoNome else linhaPrivado[1]
                            for nomePessoa, clientePessoa in zip(self.clientes.values(), self.clientes):
                                if nomeDestino == nomePessoa:
                                    clienteDestino = clientePessoa
                                    break

                            clienteDestino.send(self.codificar(
                                nomeDestino, 'MUDNPRIV', novoNome))

                        self.clientes[cliente] = novoNome

                        cliente.send(
                            self.codificar(novoNome, 'NOME', ("Nome alterado com sucesso para "+novoNome)))
                        return novoNome
                    else:
                        cliente.send(self.codificar(
                            nome, 'NOME', 'Esse nome já esta em uso'))
                        return nome

                def sairPrivado():
                    clienteDestino = None
                    for nomePessoa, clientePessoa in zip(self.clientes.values(), self.clientes):
                        if remetente == nomePessoa:
                            clienteDestino = clientePessoa
                            break
                    if self.estaEmPrivado(remetente):
                        clienteDestino.send(
                            self.codificar(remetente, 'SAIRPRIV', 'Conversa privada encerrada'))
                    for i in range(len(self.listaPrivados)):
                        if self.listaPrivados[i].count(nome):
                            if len(self.listaPrivados[i]) > 1:
                                self.listaPrivados[i].remove(nome)
                            else:
                                self.listaPrivados.remove(
                                    self.listaPrivados[i])

                def sair():
                    if self.estaEmPrivado(nome):
                        sairPrivado()
                    cliente.send(self.codificar(nome, 'SAIR', ''))
                    cliente.close()
                    del self.clientes[cliente]
                    del self.enderecos[cliente]
                    threadBroadcast = minhaThread(
                        self.broadcast, {'clienteBase': cliente, 'mensagem': nome+" saiu da conversa"})
                    print(nome+" saiu da conversa")
                    threadBroadcast.start()

                while True:
                    mensagem = cliente.recv(1024)
                    tamanho, remetente, comando, dados = self.decodificar(
                        mensagem)

                    if comando == 'NOME':
                        nome = mudarnome()

                    elif comando == 'LISTA':
                        lista()

                    elif comando == 'SAIR':
                        print(self.estaEmPrivado(nome))
                        if self.estaEmPrivado(nome):
                            sairPrivado()
                        sair()
                        break

                    elif comando == 'PRIVADO' and not self.estaEmPrivado(nome):
                        nomeDestino = dados
                        if nomeDestino == nome:
                            cliente.send(self.codificar(
                                nome, '', 'Não é possivel entrar em privado com você mesmo'))
                            return
                        clienteDestino = None
                        for nomePessoa, clientePessoa in zip(self.clientes.values(), self.clientes):
                            if nomeDestino == nomePessoa:
                                clienteDestino = clientePessoa
                                break

                        if clienteDestino is None:
                            cliente.send(self.codificar(
                                nome, '', 'Cliente não encontrado, tente novamente'))

                        clienteDestino.send(
                            self.codificar(nomeDestino, 'PRIVADO', (nome+' quer iniciar uma conversa privada com você.')))
                        clienteDestino.send(
                            self.codificar(nomeDestino, 'PRIVADO', 'Digite s para aceitar e outra coisa para negar'))
                        resposta = clienteDestino.recv(1024)
                        tamanhoResposta, remetenteResposta, comandoResposta, dadosResposta = self.decodificar(
                            resposta)

                        if dadosResposta == 's':
                            mensagemPrivada = "Conversa privada iniciada com "
                            cliente.send(
                                self.codificar(nome, '', (mensagemPrivada+nomeDestino)))
                            clienteDestino.send(
                                self.codificar(nomeDestino, '', (mensagemPrivada+nome)))

                            self.listaPrivados.append([nome, nomeDestino])

                        else:
                            cliente.send(
                                self.codificar(nome, '', ("Oferta de privado recusada!")))
                            clienteDestino.send(
                                self.codificar(nomeDestino, '', ("Oferta de privado recusada!")))

                    if(comando == 'SAIRPRIV'):
                        sairPrivado()

                    elif self.estaEmPrivado(nome):
                        for nomePessoa, clientePessoa in zip(self.clientes.values(), self.clientes):
                            if remetente == nomePessoa:
                                clienteDestino = clientePessoa
                                break
                        clienteDestino.send(
                            self.codificar(remetente, '', nome+': '+dados))

                    elif remetente == 'TODOS':
                        threadBroadcast = minhaThread(
                            self.broadcast, {'clienteBase': cliente, 'mensagem': nome+" escreveu: "+dados})
                        print(nome+": "+dados)
                        threadBroadcast.start()

            threadCliente = minhaThread(handleClient, cliente)
            threadCliente.start()

    def estaEmPrivado(self, nomeCliente):
        for linha in self.listaPrivados:
            for nome in linha:
                if nome == nomeCliente:
                    return True

        return False

    def broadcast(self, params):
        for sock, nome in zip(self.clientes.keys(), self.clientes.values()):
            if sock != params['clienteBase'] and not self.estaEmPrivado(nome):
                sock.send(self.codificar('TODOS', '', params['mensagem']))

    def codificar(self, remetente, comando, dados):
        tamanho = 2+8+8+len(dados)
        tamanhoCodificado = bytes(
            [(tamanho - (tamanho % 256))])+bytes([(tamanho % 256)])

        def transforma8bytes(x): return ('\x00'*(8-len(x))+x).encode('utf-8')

        remetenteCodificado = transforma8bytes(remetente)
        comandoCodificado = transforma8bytes(comando)

        mensagemCodificada = tamanhoCodificado + remetenteCodificado +\
            comandoCodificado + dados[0:80].encode('utf-8')
        return mensagemCodificada

    def decodificar(self, mensagem):
        tamanho = mensagem[0:2]
        mensagemDeco = mensagem.decode('utf-8')
        remetente = mensagemDeco[2:10]
        comando = mensagemDeco[10:18]
        dados = mensagemDeco[18:]

        def decode8bytes(x):
            for i in range(len(x)):
                if x[i] != '\x00':
                    return x[i:]
            return ''

        tamanhoDeco = sum(tamanho)
        remetenteDeco = decode8bytes(remetente)
        comandoDeco = decode8bytes(comando)
        dadosDeco = dados

        return tamanhoDeco, remetenteDeco, comandoDeco, dadosDeco


server = tcpServer()

server.run()
